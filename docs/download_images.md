# Downloading images produced by the Automotive SIG

The Automotive SIG provides osbuild manifests and instructions to build images
for a few platforms, currently aach64 Virtual Machine and Raspberry Pi 4.

We are also working on a process to automatically build and test these images
as we update them as well as CentOS-Stream changes. This would help us continously
deliver working images.

This is however work in progress and we cannot currently point you to existing
and downloadable images. If you would like to check our images, please follow
the instructions on the building pages.
